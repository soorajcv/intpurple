from django.contrib.auth import authenticate
from rest_framework import serializers

from account.models import User


class UserCreateSerializer(serializers.ModelSerializer):

    def create(self, validated_data):
        user = User.objects.create_user(validated_data['email'],
                                                 validated_data['phone_number'],
                                                 validated_data['first_name'],
                                                 validated_data['last_name'],
                                                 validated_data['user_name'],
                                              
                                                 validated_data['user_type'],
                                                 validated_data['qualification'],
                                                 validated_data['hospital_connected'],
                                                 validated_data['dob'],
                                                 validated_data['gender'],
                                                 validated_data['email_recovery'],
                                                 validated_data['address'],
                                                 validated_data['location'],
                                                 validated_data['pin'],
                                               

                                                 
                                                 

                                                 validated_data['password'])
        return user

    class Meta:
        model = User
        fields = ['email', 'phone_number', 'first_name','last_name','user_name','user_type','qualification','hospital_connected','dob','gender','email_recovery','address','location','country','pin','password']
        extra_kwargs = {'password': {'write_only': True}}




class UserLoginSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(max_length=255, required=True)
    password = serializers.CharField(max_length=255, required=True)

    class Meta:
        model = User
        fields = ['email', 'password']
        extra_kwargs = {'password': {'write_only': True}}

    def validate(self, attrs, ):
        user = authenticate(
            email=attrs['email'], password=attrs['password'])
        if user is None:
            raise serializers.ValidationError('invalid credentials provided')
        self.instance = user
        return user