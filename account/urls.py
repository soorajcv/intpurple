from django.conf.urls import url






from . import views
urlpatterns = [
    url(r'^users/register/$', views.UserCreateAPIView.as_view(), name='user-register'),
    url(r'^users/login/$', views.UserLoginAPIView.as_view(), name="user-login"),
]