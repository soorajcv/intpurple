from django.db import models
from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.models import BaseUserManager



class UserManager(BaseUserManager):
    """help django to work with our custom user model"""

    email_recovery=models.EmailField(max_length=255)
    email_recovery=models.EmailField(max_length=255)
    email_recovery=models.EmailField(max_length=255)
    def create_user(self,email,phone_number,first_name,last_name,user_name=None,user_type=None,qualification=None,hospital_connected=None,dob=None,gender=None,email_recovery=None,address=None,location=None,pin=None,password=None,):

        """creat a new user profile object"""
        if not email:
            raise ValueError("user must have an email")
        
        email=self.normalize_email(email=email)
        user = self.model(email=email,phone_number=phone_number,first_name=first_name,last_name=last_name,user_name=user_name,user_type=user_type,qualification=qualification,hospital_connected=hospital_connected,dob=dob,gender=gender,email_recovery=email_recovery,address=address,location=location,pin=pin,password=password)
       
        
        user.set_password(password)
        user.save(using=self._db)
        return user


    


    def create_superuser(self,email,phone_number,first_name,last_name,user_name,user_type,qualification,hospital_connected,dob,gender,email_recovery,address,location,pin,password):

        user = self.create_user(email,phone_number,first_name,last_name,user_name,user_type,qualification,hospital_connected,dob,gender,email_recovery,address,location,pin,password)
        user.is_staff=True
        user.is_superuser =True
        user.save(using=self._db)
        

        return user





















# Create your models here.

class User(AbstractBaseUser,PermissionsMixin):
    """represnt user profile table name is UserProfile"""
    user_id = models.AutoField(null=False,primary_key=True)
    phone_number = models.CharField(max_length=255,null=False)

    first_name= models.CharField(max_length=255,null=False)
    last_name = models.CharField(max_length=255,null=False)
    email= models.EmailField(max_length=255, unique=True,null=False)  #check......................check
    image= models.ImageField(null=True)
    user_name= models.CharField(max_length=255,null=True)
    password=models.CharField(max_length=255,null=False)
    password_reset_requested_at= models.DateTimeField(null=True)
    otp=models.CharField(max_length=250,null=True)
    otp_created_date = models.DateTimeField(null=True)
    is_notify_enabled= models.IntegerField(null=True)  #check...................................
    is_active=models.CharField(max_length=250,null=True)
    created_at=models.DateTimeField(null=True)
    created_by=models.CharField(max_length=250,null=True)
    modified_at=models.DateTimeField(null=True)
    modified_by=models.CharField(max_length=255,null=True)
    role=models.CharField(max_length=255,null=True)
    user_type=models.CharField(max_length=255,null=True)
    qualification=models.CharField(max_length=255,null=True)
    hospital_connected=models.CharField(max_length=255,null=True)
    dob=models.DateField(null=True)
    gender=models.CharField(max_length=255,null=True)
    email_recovery=models.EmailField(max_length=255)
    address=models.CharField(max_length=255,null=True)
    location=models.CharField(max_length=255,null=True)
    country=models.CharField(max_length=255,null=True)
    pin=models.IntegerField(null=True)
    is_staff = models.BooleanField(default=False)
    is_active=models.BooleanField(default=True)



    objects =UserManager()#want to teach django user model


    USERNAME_FIELD= 'email'
    REQUIRED_FIELDS=['phone_number','password','first_name','last_name','user_name','user_type','qualification','hospital_connected','dob','gender','email_recovery','address','location','pin']


    def __str__(self):
        return self.email





#amal sent db doc doubt


# class PermissionTable(AbstractBaseUser,PermissionsMixin):
#     permission_id=models.IntegerField(max_length=255,null=False,primary_key=True)
#     module=models.CharField(max_length=255,null=True)
#     sub_module=models.CharField(max_length=255,null=True)
#     page=models.CharField(max_length=255,null=True)
#     feature=models.CharField(max_length=255,null=True)
#     types=models.CharField(max_length=255,null=True)
#     permission=models.CharField(max_length=255,null=True)
#     description=models.CharField(max_length=255,null=True)
#     created_by=models.CharField(max_length=255,null=True)
#     created_at=models.CharField(max_length=255,null=True)
#     modified_by=models.CharField(max_length=255,null=True)
#     modified_at=models.CharField(max_length=255,null=True)


    
    


# class Roletable(AbstractBaseUser,PermissionsMixin):
#     role_id=models.IntegerField(max_length=255,null=False,primary_key=True,)
#     name=models.CharField(max_length=255,null=True)
#     description=models.CharField(max_length=255,null=True)
#     in_active=models.CharField(max_length=255,null=True)
#     created_at=models.DateTimeField(null=True)
#     created_by=models.CharField(max_length=255,null=True)
#     modified_at=models.DateTimeField(null=True)





# class Sessiontable(AbstractBaseUser,PermissionsMixin):
#     session_id=models.IntegerField(max_length=255,null=False,primary_key=True)
#     locale=models.CharField(max_length=255,null=True)
#     login_time=models.DateTimeField(null=True)
#     logout_time=models.DateTimeField(null=True)
#     entity=models.CharField(max_length=255,null=True)
#     entity_id=models.IntegerField(max_length=255,null=False)
#     last_used_on=models.DateTimeField(null=True)
#     is_active=models.IntegerField(max_length=255,null=False)
#     created_at=models.DateTimeField(null=True)

























